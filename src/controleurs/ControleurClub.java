
package controleurs;

//INSERT IMPORT
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ControleurClub {
   
    Connection  cx;
    
    public void init() throws Exception{
    
          cx= connexionBdd();
          if (cx!=null){ System.out.println("CONNEXION OK");}
    
    }
    
    
    Connection connexionBdd() throws Exception {
        
        Connection connexion=null;
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        connexion =  DriverManager.getConnection("jdbc:derby://localhost:1527/BaseFederationJudo","uJudo","mdp") ;
        return connexion;
    }  
    
        //VARIABLES DE LIAISON
        String codeclub;
        String nomclub;
        String adresse;
        int effectif;
        int effectifemmes;
        int effectifhommes;
        float agemoy;
        float poidsmoyfemmes;
        float poidsmoyhommes;
        
        //VARIABLES DE CALCULS
        int effTotal;
        int effMasc;
        int effFemi;
        
   
    
    public void rechercher() throws SQLException {
  
       String requete= "Select  nomclub, adrclub " +
                       "From   CLUB " +
                       "Where CODECLUB= '" +getCodeclub()+"'";
       
             Statement   cmde   = cx.createStatement();
             ResultSet   pers   = cmde.executeQuery(requete); 
             
             pers.next();
             setNomclub(pers.getString(1));
             setAdresse(pers.getString(2));
           
             
             
       String requete2= "Select * " +
                        "From JUDOKA INNER JOIN CLUB ON JUDOKA.leclub_codeclub = CLUB.codeclub " +
                        "Where codeclub= '" +getCodeclub()+"'";
               
             Statement   cmde2   = cx.createStatement();
             ResultSet   pers2   = cmde2.executeQuery(requete2);
             
             while(pers2.next()){
                 
             }
             
               
             
}
    //<editor-fold defaultstate="collapsed" desc="hidden">
    public static final String PROP_CODECLUB = "codeclub";
    
    public String getCodeclub() {
        return codeclub;
    }
    
    public void setCodeclub(String codeclub) {
        String oldCodeclub = this.codeclub;
        this.codeclub = codeclub;
        propertyChangeSupport.firePropertyChange(PROP_CODECLUB, oldCodeclub, codeclub);
    }
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    public static final String PROP_NOMCLUB = "nomclub";
    
    public String getNomclub() {
        return nomclub;
    }
    
    public void setNomclub(String nomclub) {
        String oldNomclub = this.nomclub;
        this.nomclub = nomclub;
        propertyChangeSupport.firePropertyChange(PROP_NOMCLUB, oldNomclub, nomclub);
    }
    
    public static final String PROP_ADRESSE = "adresse";

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        String oldAdresse = this.adresse;
        this.adresse = adresse;
        propertyChangeSupport.firePropertyChange(PROP_ADRESSE, oldAdresse, adresse);
    }
    
    public static final String PROP_EFFECTIF = "effectif";

    public int getEffectif() {
        return effectif;
    }

    public void setEffectif(int effectif) {
        int oldEffectif = this.effectif;
        this.effectif = effectif;
        propertyChangeSupport.firePropertyChange(PROP_EFFECTIF, oldEffectif, effectif);
    }
    
    public static final String PROP_EFFECTIFEMMES = "effectifemmes";

    public int getEffectifemmes() {
        return effectifemmes;
    }

    public void setEffectifemmes(int effectifemmes) {
        int oldEffectifemmes = this.effectifemmes;
        this.effectifemmes = effectifemmes;
        propertyChangeSupport.firePropertyChange(PROP_EFFECTIFEMMES, oldEffectifemmes, effectifemmes);
    }
    
    public static final String PROP_EFFECTIFHOMMES = "effectifhommes";

    public int getEffectifhommes() {
        return effectifhommes;
    }

    public void setEffectifhommes(int effectifhommes) {
        int oldEffectifhommes = this.effectifhommes;
        this.effectifhommes = effectifhommes;
        propertyChangeSupport.firePropertyChange(PROP_EFFECTIFHOMMES, oldEffectifhommes, effectifhommes);
    }
    
    public static final String PROP_AGEMOY = "agemoy";

    public float getAgemoy() {
        return agemoy;
    }

    public void setAgemoy(float agemoy) {
        float oldAgemoy = this.agemoy;
        this.agemoy = agemoy;
        propertyChangeSupport.firePropertyChange(PROP_AGEMOY, oldAgemoy, agemoy);
    }
    
    public static final String PROP_POIDSMOYFEMMES = "poidsmoyfemmes";

    public float getPoidsmoyfemmes() {
        return poidsmoyfemmes;
    }

    public void setPoidsmoyfemmes(float poidsmoyfemmes) {
        float oldPoidsmoyfemmes = this.poidsmoyfemmes;
        this.poidsmoyfemmes = poidsmoyfemmes;
        propertyChangeSupport.firePropertyChange(PROP_POIDSMOYFEMMES, oldPoidsmoyfemmes, poidsmoyfemmes);
    }
    
    public static final String PROP_POIDSMOYHOMMES = "poidsmoyhommes";

    public float getPoidsmoyhommes() {
        return poidsmoyhommes;
    }

    public void setPoidsmoyhommes(float poidsmoyhommes) {
        float oldPoidsmoyhommes = this.poidsmoyhommes;
        this.poidsmoyhommes = poidsmoyhommes;
        propertyChangeSupport.firePropertyChange(PROP_POIDSMOYHOMMES, oldPoidsmoyhommes, poidsmoyhommes);
    }
    //</editor-fold>
}
