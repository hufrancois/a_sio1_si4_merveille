
package controleurs;


import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ControleurJudokas {
    
    Connection  cx;

    public void init() throws Exception{
    
          cx= connexionBdd();
          if (cx!=null){ System.out.println("CONNEXION OK");}
    
    }
    
     
    Connection connexionBdd() throws Exception{
   
        Connection connexion=null;
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        connexion =  DriverManager.getConnection("jdbc:derby://localhost:1527/BaseFederationJudo","uJudo","mdp") ;
        return connexion;
    }  
    
    
        String nom;
        String prenom;
        String sexe;
        String club;
        String dateDeNaissance;
        int age;
        float poids;
        float taille;
        float imc;
        String catPoids;
        String ceinture;
        int nombreVictoires;
        long licence;
        float x;

        public void rechercher() throws SQLException {
            
             String requete= "Select  nom, prenom, sexe, leclub_codeclub, datenaiss, poids, taille, ceinture, nbVictoires " +
                             "From   JUDOKA "    +
                             "Where id= " +getLicence();
            
             Statement   cmde   = cx.createStatement();
             ResultSet   pers   = cmde.executeQuery(requete);
             
             pers.next();
             setNom(pers.getString(1));
             setPrenom(pers.getString(2));
             setSexe(pers.getString(3));
             setClub(pers.getString(4));
             setDateDeNaissance(pers.getString(5));
             setAge(utilitaires.UtilDate.ageEnAnnees(pers.getDate(5)));
             setPoids(pers.getFloat(6));
             setTaille(pers.getFloat(7)/100);
             x = (poids/(taille*taille));
             setImc(x);
             setCatPoids(utilitaires.UtilDojo.determineCategorie(pers.getString(3),pers.getInt(6)));
             setCeinture(pers.getString(8));
             setNombreVictoires(pers.getInt(9));
           
             
        }
   
        //<editor-fold defaultstate="collapsed" desc="hidden">
        public static final String PROP_NOM = "nom";
        
        public String getNom() {
            return nom;
        }
        
        public void setNom(String nom) {
            String oldNom = this.nom;
            this.nom = nom;
            propertyChangeSupport.firePropertyChange(PROP_NOM, oldNom, nom);
        }
        private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
        
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(listener);
        }
        
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(listener);
        }
        
        public static final String PROP_PRENOM = "prenom";
        
        public String getPrenom() {
            return prenom;
        }
        
        public void setPrenom(String prenom) {
            String oldPrenom = this.prenom;
            this.prenom = prenom;
            propertyChangeSupport.firePropertyChange(PROP_PRENOM, oldPrenom, prenom);
        }
        
        public static final String PROP_SEXE = "sexe";
        
        public String getSexe() {
            return sexe;
        }
        
        public void setSexe(String sexe) {
            String oldSexe = this.sexe;
            this.sexe = sexe;
            propertyChangeSupport.firePropertyChange(PROP_SEXE, oldSexe, sexe);
        }
        
        public static final String PROP_CLUB = "club";
        
        public String getClub() {
            return club;
        }
        
        public void setClub(String club) {
            String oldClub = this.club;
            this.club = club;
            propertyChangeSupport.firePropertyChange(PROP_CLUB, oldClub, club);
        }
        
        public static final String PROP_DATEDENAISSANCE = "dateDeNaissance";
        
        public String getDateDeNaissance() {
            return dateDeNaissance;
        }
        
        public void setDateDeNaissance(String dateDeNaissance) {
            String oldDateDeNaissance = this.dateDeNaissance;
            this.dateDeNaissance = dateDeNaissance;
            propertyChangeSupport.firePropertyChange(PROP_DATEDENAISSANCE, oldDateDeNaissance, dateDeNaissance);
        }
        
        public static final String PROP_AGE = "age";
        
        public int getAge() {
            return age;
        }
        
        public void setAge(int age) {
            int oldAge = this.age;
            this.age = age;
            propertyChangeSupport.firePropertyChange(PROP_AGE, oldAge, age);
        }
        
         public static final String PROP_POIDS = "poids";

         public float getPoids() {
             return poids;
        }

         public void setPoids(float poids) {
             float oldPoids = this.poids;
             this.poids = poids;
             propertyChangeSupport.firePropertyChange(PROP_POIDS, oldPoids, poids);
        }
        
         public static final String PROP_TAILLE = "taille";

         public float getTaille() {
             return taille;
        }

        public void setTaille(float taille) {
            float oldTaille = this.taille;
            this.taille = taille;
            propertyChangeSupport.firePropertyChange(PROP_TAILLE, oldTaille, taille);
        }
        
        public static final String PROP_IMC = "imc";
        
        public float getImc() {
            return imc;
        }
        
        public void setImc(float imc) {
            float oldImc = this.imc;
            this.imc = imc;
            propertyChangeSupport.firePropertyChange(PROP_IMC, oldImc, imc);
        }
        
        public static final String PROP_CATPOIDS = "catPoids";
        
        public String getCatPoids() {
            return catPoids;
        }
        
        public void setCatPoids(String catPoids) {
            String oldCatPoids = this.catPoids;
            this.catPoids = catPoids;
            propertyChangeSupport.firePropertyChange(PROP_CATPOIDS, oldCatPoids, catPoids);
        }
        
       public static final String PROP_CEINTURE = "ceinture";

         public String getCeinture() {
             return ceinture;
        }

         public void setCeinture(String ceinture) {
             String oldCeinture = this.ceinture;
             this.ceinture = ceinture;
             propertyChangeSupport.firePropertyChange(PROP_CEINTURE, oldCeinture, ceinture);
        }
        
        public static final String PROP_NOMBREVICTOIRES = "nombreVictoires";
        
        public int getNombreVictoires() {
            return nombreVictoires;
        }
        
        public void setNombreVictoires(int nombreVictoires) {
            int oldNombreVictoires = this.nombreVictoires;
            this.nombreVictoires = nombreVictoires;
            propertyChangeSupport.firePropertyChange(PROP_NOMBREVICTOIRES, oldNombreVictoires, nombreVictoires);
        }
        
        public static final String PROP_LICENCE = "licence";

         public long getLicence() {
              return licence;
         }
         
         public void setLicence(long licence) {
             long oldLicence = this.licence;
             this.licence = licence;
             propertyChangeSupport.firePropertyChange(PROP_LICENCE, oldLicence, licence);
         }
        //</editor-fold>
    
}
